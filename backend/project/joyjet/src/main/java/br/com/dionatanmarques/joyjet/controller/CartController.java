package br.com.dionatanmarques.joyjet.controller;

import javax.inject.Inject;

import br.com.caelum.vraptor.Consumes;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.serialization.gson.WithoutRoot;
import br.com.caelum.vraptor.view.Results;
import br.com.dionatanmarques.joyjet.model.Checkout;

@Controller
public class CartController {

	@Inject
	private Result result;

	@Post
	@Path("/level1")
	@Consumes(value="application/json", options=WithoutRoot.class)
	public void level1(Checkout checkout) {
		result.use(Results.json()).from(checkout.getCarts(), "carts").serialize();
	}
}