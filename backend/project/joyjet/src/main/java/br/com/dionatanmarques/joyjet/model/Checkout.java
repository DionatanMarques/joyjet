package br.com.dionatanmarques.joyjet.model;

import java.util.List;

public class Checkout {

	private List<Article> articles;
	private List<Cart> carts;

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public List<Cart> getCarts() {
		return carts;
	}

	public void setCarts(List<Cart> carts) {
		this.carts = carts;
	}
}